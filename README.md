# guardtime

> Documents project

# Documents Project

**Created with:**

- Vue.js
- Vuex
- Webpack
- Less

## Build Setup

```bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev
```

**Required**

- Node
- Webpack
