import Vue from 'vue';
import Vuex from 'vuex';

import {
    requestService
} from "./services";

Vue.use(Vuex);

const appModule = {
    state: {
        documents: {},
        validations: {},
        metaInfo: {}
    },
    getters: {
        documents: state => {
            return state.documents;
        },
        validations: state => {
            return state.validations;
        },
        meta: state => {
            return state.metaInfo;
        }
    },
    mutations: {
        SET_META_INFO(state, meta) {
            state.metaInfo = meta;
        },
        SET_STATUS_DATA(state, data) {
            Vue.set(state.validations, data.id, data);
        },
        UPDATE_DOCUMENT(state, data) {
            let document = data.document;
            document.status = data.status;
            Vue.set(state.documents, document.id, document);
        },
        SET_DOCUMENTS(state, documents) {
            for (let i = 0; i < documents.length; i++) {
                let dock = documents[i];
                dock.status = 0;
                Vue.set(state.documents, dock.id, dock);
            }
        }
    },
    actions: {
        validateDocument(context, document) {
            let requests = [
                requestService.validateCheckSum,
                requestService.validateSchema,
                requestService.validateSignature
            ];

            context.commit('SET_STATUS_DATA', {
                step: -1,
                id: document.id,
                status: 'pending'
            });

            const validate = () => {
                let index = 0;

                const call = () => {
                    return requests[index](document.id).then(data => {
                        if (!data.valid) {
                            context.commit('SET_STATUS_DATA', {
                                step: index,
                                id: document.id,
                                status: 'invalid'
                            });
                            return;
                        }
                        index++;
                        if (index == requests.length) {
                            context.commit('SET_STATUS_DATA', {
                                step: index,
                                id: document.id,
                                status: 'valid'
                            });
                            return 'done';
                        }
                        return call();
                    }, error => {
                        context.commit('SET_STATUS_DATA', {
                            step: index,
                            id: document.id,
                            status: 'error',
                            message: error
                        });
                    })
                }

                return call();
            }
            validate(requests);
        },

        getDocuments(context, page = 1) {
            return new Promise((resolve, reject) => {
                requestService.getDocumentList(page).then(result => {
                    context.commit('SET_DOCUMENTS', result.data);
                    context.commit('SET_META_INFO', result.meta);
                    resolve();
                }, error => {
                    reject(error);
                });
            })
        }
    }
}


const store = new Vuex.Store({
    modules: {
        appModule
    },
    strict: true,
    plugins: []
});

export default store;