import axios from 'axios';

export default class Requests {
    constructor() {
        this.request = axios.create({
            baseURL: 'http://fe-test.guardtime.com'
        })

        this.request.interceptors.response.use((response) => {
            return response.data;
        }, (error) => {
            return Promise.reject(error.response.data);
        });

        this.validateCheckSum = this.validateCheckSum.bind(this);
        this.validateSchema = this.validateSchema.bind(this);
        this.validateSignature = this.validateSignature.bind(this);
    }

    getDocumentList(page = 1, perPage = 20) {
        let params = {
            page,
            perPage
        };
        return this.request.get('/documents', {
            params
        });
    }

    validateCheckSum(id = null) {
        return this.request.post(['/documents', id, 'validateChecksum'].join('/'));
    }

    validateSchema(id = null) {
        return this.request.post(['/documents', id, 'validateSchema'].join('/'));
    }

    validateSignature(id = null) {
        return this.request.post(['/documents', id, 'validateSignature'].join('/'));
    }
}